$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    })
    $('#contacto').on('show.bs.modal', function(e){
        console.log('El modal se esta mostrando');
    $('#contactoBtn').removeClass('btn-outline-success');
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('El modal se esta ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('El modal se ocultó');
    });
});